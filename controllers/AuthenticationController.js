// Definición de dependencias
const bcrypt = require('bcrypt');
const nconf = require('nconf');
const requestJson = require('request-json');
const winston = require('./../config/winston');


// Configuración conexión mLab
nconf.file({ file: './config/bbconfig.json' });
const basesMLabURL = nconf.get('mLab:basesMLabURL');
const mLabAPIKey = nconf.get('mLab:mLabAPIKey');



// Encripta una cadena con 10 rondas de SALTeado
function hash(data) {
  winston.info("Hashing data");

  return bcrypt.hashSync(data, 10);
}


// Valida si sentPassword es igual que hashedPassword
function checkPassword(sentPassword, hashedPassword) {
  winston.info("Checking password");

  return bcrypt.compareSync(sentPassword, hashedPassword);
}



// GET Listado de credenciales existentes
function getCredentialsV1(req, res) {
  winston.info("GET /blockbanks/v1/credential");

  var httpClient = requestJson.createClient(basesMLabURL);
  winston.info("HTTP client created succesfully");

  httpClient.get("credential?" + mLabAPIKey,
        function(err, resMLab, body) {
          var response = !err ? body : { "msg" : "Error getting credentials" };
          res.send(response);
        }
      )
}


// GET Listado de credencial por userid
function getCredentialByUserIdV1(req, res) {
  winston.info("GET /blockbanks/v1/credential/:userid");

  var httpClient = requestJson.createClient(basesMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.userid;
  var query = 'q={"userid":' + id + '}';
  httpClient.get("credential?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting credential");
            var response = { "msg" : "Error getting user credential" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body[0];
            } else {
              winston.warn("User not found");
              var response = { "msg" : "User not found" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}


// Exports de funciones
module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
// Exports de endpoints
module.exports.getCredentialsV1 = getCredentialsV1;
module.exports.getCredentialByUserIdV1 = getCredentialByUserIdV1;
