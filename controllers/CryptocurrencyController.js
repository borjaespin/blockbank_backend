// Definición de dependencias
const nconf = require('nconf');
const requestJson = require('request-json');
const winston = require('./../config/winston');


// Configuración conexión mLab
nconf.file({ file: './config/bbconfig.json' });
const baseMLabURL = nconf.get('mLab:baseMLabURL');
const mLabAPIKey = nconf.get('mLab:mLabAPIKey');



// Convierte el nombre corto de criptomoneda a su identificador
function convertCryptocurrencyToId (ccstr) {
  if (ccstr == "BTC")        { return(1); }
  else if (ccstr == "ETH")   { return(2); }
  else if (ccstr == "XRP")   { return(3); }
  else if (ccstr == "DOGE")  { return(4); }
  else if (ccstr == "PUT")   { return(5); }
  else                       { return(-1); }
}


// Convierte el identificador de criptomoneda a su nombre corto
function convertIdToCryptocurrency (ccid) {
  if (ccid == "1")      { return(BTC); }
  else if (ccid == "2") { return(ETH); }
  else if (ccid == "3") { return(XRP); }
  else if (ccid == "4") { return(DOGE); }
  else if (ccid == "5") { return(PUT); }
  else                  { return(-1); }
}


// Comprueba si el formato del parametro pasado es un numero (=id)
function isNumericParam (ccparam) {
  return (!isNaN(ccparam));
}



// GET Listado de criptomonedas existentes
function getCryptocurrencyListV1(req, res) {
  winston.info("GET /blockbank/v1/cryptocurrency");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  httpClient.get("cryptocurrency?" + mLabAPIKey,
        function(err, resMLab, body) {
          var response = !err ? body : { "msg" : "Error getting cryptocurrencies" };
          res.send(response);
        }
      )
}


// GET Listado de una criptomoneda por cryptocurrency
function getCryptocurrencyV1(req, res) {
  winston.info("GET /blockbank/v1/cryptocurrency/:cryptocurrency");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var ccparam = req.params.cryptocurrency;
  if (!isNumericParam(req.params.cryptocurrency))
  {
    ccparam = convertCryptocurrencyToId(ccparam);
  }

  var query = 'q={"cryptocurrencyid":' + ccparam + '}';
  httpClient.get("cryptocurrency?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting cryptocurrency");
            var response = { "msg" : "Error getting cryptocurrency" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body[0];
            } else {
              winston.warn("Cryptocurrency not found");
              var response = { "msg" : "Cryptocurrency not found" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}


// POST Listado de criptomoneda por cryptocurrency
function updateCryptocurrencyV1(req, res) {
  winston.info("POST /blockbank/v1/cryptocurrency/:cryptocurrency");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var ccparam = req.params.cryptocurrency;
  if (!isNumericParam(req.params.cryptocurrency))
  {
    ccparam = convertCryptocurrencyToId(ccparam);
  }

  var query = 'q={"cryptocurrencyid":' + ccparam + '}';
  httpClient.get("cryptocurrency?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting cryptocurrency");
            var response = { "msg" : "Error getting cryptocurrency" };
            res.status(500);
            res.send(response);
          } else {
            if (body.length == 0) {
              winston.warn("Cryptocurrency not found");
              var response = { "msg" : "Cryptocurrency not found" };
              res.status(404);
              res.send(response);
            } else {
              query = 'q={"cryptocurrencyid" :' + body[0].cryptocurrencyid + '}';

              var putBody = {
                  	"$set" : {
                        "name"      : req.body.name,
                        "shortname" : req.body.shortname,
                        "eurvalue"  : req.body.eurvalue
                  	}
              }

              httpClient.put("cryptocurrency?" + query + "&" + mLabAPIKey, putBody,
                    function(errPUT, resMLabPUT, bodyPUT) {
                      winston.info("Cryptocurrency succesfully updated");
                      var response = { "msg" : "Cryptocurrency succesfully updated" };
                      res.send(response);
                }
              )
            }
          }
        }
    )
}



// Exports de funciones
module.exports.convertCryptocurrencyToId = convertCryptocurrencyToId;
module.exports.convertIdToCryptocurrency = convertIdToCryptocurrency;
module.exports.isNumericParam = isNumericParam;
// Exports de endpoints
module.exports.getCryptocurrencyListV1 = getCryptocurrencyListV1;
module.exports.getCryptocurrencyV1 = getCryptocurrencyV1;
module.exports.updateCryptocurrencyV1 = updateCryptocurrencyV1;
