// Definición de dependencias
const nconf = require('nconf');
const requestJson = require('request-json');
const randomString = require('random-string');
const winston = require('./../config/winston');

// Definición de controladores
const cryptocurrencyController = require('./CryptocurrencyController.js');


// Configuración conexión mLab
nconf.file({ file: './config/bbconfig.json' });
const baseMLabURL = nconf.get('mLab:baseMLabURL');
const mLabAPIKey = nconf.get('mLab:mLabAPIKey');



// GET Listado de cuentas existentes
function getAccountListV1(req, res) {
  winston.info("GET /blockbank/v1/account");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  httpClient.get("account?" + mLabAPIKey,
        function(err, resMLab, body) {
          var response = !err ? body : { "msg" : "Error getting accounts" };
          res.send(response);
        }
      )
}


// GET Listado de una cuenta por accountid
function getAccountByIdV1(req, res) {
  winston.info("GET /blockbank/v1/account/:accountid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.accountid;
  var query = 'q={"accountid":' + id + '}';
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting account");
            var response = { "msg" : "Error getting account" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body[0];
            } else {
              winston.warn("Account not found");
              var response = { "msg" : "Account not found" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}


// GET Listado de cuentas por userid
function getAccountsByUserV1(req, res) {
  winston.info("GET /blockbank/v1/accountsuser/:userid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.userid;
  var query = 'q={"userid":' + id + '}';
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting accounts");
            var response = { "msg" : "Error getting accounts" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body;
            } else {
              winston.warn("User does not have any accounts");
              var response = { "msg" : "User does not have any accounts" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}


// GET Listado de cuentas por userid y cryptocurrency
function getAccountByUserCryptocurrencyV1(req, res) {
  winston.info("GET /blockbank/v1/accountusercryptocurrency/:userid/:cryptocurrency");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var ccparam = req.params.cryptocurrency;
  if (!cryptocurrencyController.isNumericParam(req.params.cryptocurrency))
  {
    ccparam = cryptocurrencyController.convertCryptocurrencyToId(ccparam);
  }

  var uid = req.params.userid;
  var query = 'q={"userid":' + uid + ',"cryptocurrencyid":' + ccparam + '}';
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting accounts");
            var response = { "msg" : "Error getting accounts" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body;
            } else {
              winston.warn("User does not have that cryptocurrency");
              var response = { "msg" : "User does not have that cryptocurrency" };
              res.status(404);
            }
          }
          res.send(response);
        }
    )
}


// POST Alta de nueva cuenta de usuario por criptomoneda
function createAccountByUserCryptocurrencyV1(req, res) {
  winston.info("POST /blockbank/v1/accountusercryptocurrency/:userid/:cryptocurrency");
  winston.info(req.headers);

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  httpClient.get("account?" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error creating account");
            var response = { "msg" : "Error creating account" };
            res.status(500);
          } else {
            var maxAccountId = 0;
            var alreadyExists = false;
            var ccparam = req.params.cryptocurrency;
            if (!cryptocurrencyController.isNumericParam(req.params.cryptocurrency))
            {
              ccparam = cryptocurrencyController.convertCryptocurrencyToId(ccparam);
            }

            for (const account of body) {
              if (parseInt(account.accountid) > maxAccountId) {
                maxAccountId = parseInt(account.accountid);
              }

              if (account.userid == parseInt(req.params.userid) &&
                  account.cryptocurrencyid == ccparam) {
                    alreadyExists = true;
                  }
            }

            if (alreadyExists) {
              winston.error("User already has that cryptocurrency");
              var response = { "msg" : "User already has an account contracted with that cryptocurrency" };
              res.status(500);
              res.send(response);
            } else {
              var newAccount = {
                "accountid"         : parseInt(maxAccountId + 1),
                "userid"            : parseInt(req.params.userid),
                "cryptocurrencyid"  : ccparam,
                "address"           : randomString({length: 34}),
                "balance"            : 0,
                "active"            : true
              };

              httpClient.post("account?"  + mLabAPIKey, newAccount,
                    function(err, resMLab, body) {
                      if (err) {
                        winston.error("Error creating account");
                        var response = { "msg" : "Error creating account" };
                        res.status(500);
                      } else {
                        winston.info("Account succesfully created");
                        var response = { "msg" : "Account succesfully created" };
                      }
                      res.send(response);
                    }
              )
            }
          }
        }
      )
}


// POST Actualizacion de cuenta por accountid
function updateAccountV1(req, res) {
  winston.info("POST /blockbank/v1/account/:accountid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.accountid;
  var query = 'q={"accountid":' + id + '}';
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting account");
            var response = { "msg" : "Error getting account" };
            res.status(500);
            res.send(response);
          } else {
            if (body.length == 0) {
              winston.warn("Account not found");
              var response = { "msg" : "Account not found" };
              res.status(404);
              res.send(response);
            } else {
              query = 'q={"accountid" :' + body[0].accountid + '}';

              var putBody = {
                  	"$set" : {
                        "userid"            : req.body.userid,
                        "cryptocurrencyid"  : req.body.cryptocurrencyid,
                        "address"           : req.body.address,
                        "balance"            : req.body.balance,
                        "active"            : req.body.active
                  	}
              }

              httpClient.put("account?" + query + "&" + mLabAPIKey, putBody,
                    function(errPUT, resMLabPUT, bodyPUT) {
                      winston.info("Account succesfully updated");
                      var response = { "msg" : "Account succesfully updated" };
                      res.send(response);
                    }
              )
            }
          }
        }
    )
}


// POST Actualizacion de cuenta por userdid y criptomoneda
function updateAccountByUserCryptocurrencyV1(req, res) {
  winston.info("POST /blockbank/v1/accountusercryptocurrency/:userid/:cryptocurrency/:balance");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var ccparam = req.params.cryptocurrency;
  if (!cryptocurrencyController.isNumericParam(req.params.cryptocurrency))
  {
    ccparam = cryptocurrencyController.convertCryptocurrencyToId(ccparam);
  }

  var uid = req.params.userid;
  var query = 'q={"userid":' + uid + ',"cryptocurrencyid":' + ccparam + '}';
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting account");
            var response = { "msg" : "Error getting account" };
            res.status(500);
            res.send(response);
          } else {
            if (body.length == 0) {
              winston.warn("User account not found");
              var response = { "msg" : "User account not found" };
              res.status(404);
              res.send(response);
            } else {
              var updatedBalance = parseFloat(parseFloat(body[0].balance) + parseFloat(req.params.balance)).toFixed(2);
              query = 'q={"accountid" :' + body[0].accountid + '}';
              var putBody = {"$set" : { "balance" : updatedBalance }};
              httpClient.put("account?" + query + "&" + mLabAPIKey, putBody,
                    function(errPUT, resMLabPUT, bodyPUT) {
                      winston.info("Account succesfully updated");
                      var response = { "msg" : "Account succesfully updated" };
                      res.send(response);
                    }
              )
            }
          }
        }
    )
}


// DELETE Borrado (baja logica) de cuenta por accountid
function deleteAccountByIdV1(req, res) {
  winston.info("DELETE /blockbank/v1/account/:accountid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.accountid;
  var query = 'q={"accountid":' + id + '}';
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (body.length == 0) {
            winston.error("Account does not exists");
            var response = { "msg" : "Account does not exists" };
            res.status(500);
            res.send(response);
          } else {
            query = 'q={"accountid" :' + body[0].accountid + '}';
            var putBody = '{"$set" : {"active" : false }}';
            httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                  function(errPUT, resMLabPUT, bodyPUT) {
                    winston.info("Account succesfully deleted");
                    var response = { "msg" : "Account succesfully deleted" };
                    res.send(response);
                  }
            )
          }
        }
    )
}


// DELETE Borrado (baja logica) de cuentas por userid
function deleteAccountsByUserV1(req, res) {
  winston.info("DELETE /blockbank/v1/accountsuser/:userid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.userid;
  var query = 'q={"userid":' + id + '}';
  httpClient.get("account?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting accounts");
            var response = { "msg" : "Error getting accounts" };
            res.status(500);
          } else {
            if (body.length > 0) {
              for (account of body) {
                query = 'q={"accountid" :' + account.accountid + '}';
                var putBody = '{"$set" : {"active" : false }}';
                httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                      function(errPUT, resMLabPUT, bodyPUT) {
                        winston.info("Account succesfully deleted");
                      }
                )
                var response = { "msg" : "Accounts successfully updated" };
              }
            } else {
              winston.warn("User does not have any accounts");
              var response = { "msg" : "User does not have any accounts" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}



// Exports de endpoints
module.exports.getAccountListV1 = getAccountListV1;
module.exports.getAccountByIdV1 = getAccountByIdV1;
module.exports.getAccountsByUserV1 = getAccountsByUserV1;
module.exports.getAccountByUserCryptocurrencyV1 = getAccountByUserCryptocurrencyV1;
module.exports.createAccountByUserCryptocurrencyV1 = createAccountByUserCryptocurrencyV1;
module.exports.updateAccountV1 = updateAccountV1;
module.exports.updateAccountByUserCryptocurrencyV1 = updateAccountByUserCryptocurrencyV1;
module.exports.deleteAccountByIdV1 = deleteAccountByIdV1;
module.exports.deleteAccountsByUserV1 = deleteAccountsByUserV1;
