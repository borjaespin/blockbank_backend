// Definición de dependencias
const nconf = require('nconf');
const requestJson = require('request-json');
const winston = require('./../config/winston');

// Definición de controladores
const authenticationController = require('./AuthenticationController');


// Configuración conexión mLab
nconf.file({ file: './config/bbconfig.json' });
const baseMLabURL = nconf.get('mLab:baseMLabURL');
const basesMLabURL = nconf.get('mLab:basesMLabURL');
const mLabAPIKey = nconf.get('mLab:mLabAPIKey');



// Funcion de login de usuario
function loginV1(req, res) {
  winston.info("POST /blockbank/v1/login");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var query = 'q={"email": "' + req.body.email + '"}';
  httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting user");
            var response = { "msg" : "Error getting user" };
            res.status(500);
            res.send(response);
          } else if (body.length <=0) {
            winston.warn("User not found");
            var response = { "msg" : "User not found" };
            res.status(404);
            res.send(response);
          } else {
            query = 'q={"userid" :' + body[0].userid + '}';

            var httpClients = requestJson.createClient(basesMLabURL);
            winston.info("HTTP client created succesfully");

            httpClients.get("credential?" + query + "&" + mLabAPIKey,
                  function(err, resMLab, credentialBody) {
                    if (err) {
                      winston.error("Error getting user");
                      var response = { "msg" : "Error getting user" };
                      res.status(500);
                      res.send(response);
                    } else if (credentialBody.length <=0) {
                      winston.warn("User credentials not found");
                      var response = { "msg" : "User credentials not found" };
                      res.status(404);
                      res.send(response);
                    } else {
                        if (!authenticationController.checkPassword(req.body.password, credentialBody[0].password)) {
                          var response = { "mensaje" : "Incorrect login data"} ;
                          res.status(400);
                          res.send(response);
                        } else {

                          query = 'q={"userid" :' + body[0].userid + '}';
                          var putBody = {"$set" : { "connected" : true }};
                          httpClient.put("user?" + query + "&" + mLabAPIKey, putBody,
                            function(errPUT, resMLabPUT, bodyPUT) {
                              winston.info("User successfully logged in");
                              var response = { "msg" : "User successfully logged in",
                                               "userid" : body[0].userid };
                              res.send(response);
                          })
                        }
                      }
                  })
            }
    })
}


// Funcion de logout de usuario
function logoutV1(req, res) {
  winston.info("POST /blockbank/v1/logout");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var query = 'q={"email": "' + req.body.email + '"}';
  httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting user");
            var response = { "msg" : "Error getting user" };
            res.status(500);
            res.send(response);
          } else if (body.length <=0) {
            winston.warn("User not found");
            var response = { "msg" : "User not found" };
            res.status(404);
            res.send(response);
          } else {
            // Check if user is logged
            if (!body[0].connected) {
              var response = { "mensaje" : "User currently not logged"} ;
              res.status(400);
              res.send(response);
            } else {
              query = 'q={"userid" :' + body[0].userid + '}';
              var putBody = {"$set" : { "connected" : false }};
              httpClient.put("user?" + query + "&" + mLabAPIKey, putBody,
                function(errPUT, resMLabPUT, bodyPUT) {
                  winston.info("User successfully logged out");
                  var response = { "msg" : "User successfully logged out" };
                  res.send(response);
              })
            }
          }
    })
}



// Exports de endpoints
module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
