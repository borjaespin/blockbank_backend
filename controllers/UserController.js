// Definición de dependencias
const nconf = require('nconf');
const requestJson = require('request-json');
const winston = require('./../config/winston');

// Configuración conexión mLab
nconf.file({ file: './config/bbconfig.json' });
const baseMLabURL = nconf.get('mLab:baseMLabURL');
const basesMLabURL = nconf.get('mLab:basesMLabURL');
const mLabAPIKey = nconf.get('mLab:mLabAPIKey');

// Definición de controladores
const authenticationController = require('./AuthenticationController');



// GET Listado de usuarios existentes
function getUserListV1(req, res) {
  winston.info("GET /blockbank/v1/users");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  httpClient.get("user?" + mLabAPIKey,
        function(err, resMLab, body) {
          var response = !err ? body : { "msg" : "Error getting users" };
          res.send(response);
        }
      )
}


// GET Listado de un usuario por userid
function getUserByIdV1(req, res) {
  winston.info("GET /blockbank/v1/user/:userid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.userid;
  var query = 'q={"userid":' + id + '}';
  httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting user");
            var response = { "msg" : "Error getting user" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body[0];
            } else {
              winston.warn("User not found");
              var response = { "msg" : "User not found" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}


// POST Alta de nuevo usuario
function createUserV1(req, res) {
  winston.info("POST /blockbank/v1/user");
  winston.info(req.headers);

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  httpClient.get("user?" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error creating user");
            var response = { "msg" : "Error creating user" };
            res.status(500);
            res.send(response);
          } else {
            var maxUserId = 0;
            var alreadyExists = false;
            for (const user of body) {
              if (parseInt(user.userid) > maxUserId) {
                maxUserId = parseInt(user.userid);
              }
              if (user.email == req.body.email) {
                alreadyExists = true;
              }
            }

            if (alreadyExists) {
              winston.error("User already exists");
              var response = { "msg" : "User already exists" };
              res.status(409);
              res.send(response);
            } else {

              var newUser = {
                "userid"    : parseInt(maxUserId + 1),
                "name"      : req.body.name,
                "surname"   : req.body.surname,
                "address"   : req.body.address,
                "cellphone" : req.body.cellphone,
                "email"     : req.body.email,
                "timezone"  : req.body.timezone,
                "languaje"  : req.body.languaje,
                "connected" : false,
                "active"    : true
              }

              httpClient.post("user?"  + mLabAPIKey, newUser,
                    function(err, resMLab, body) {
                      if (err) {
                        winston.error("Error creating user");
                        var response = { "msg" : "Error creating user" };
                        res.status(500);
                        res.send(response);
                      } else {
                        winston.info("User succesfully created");

                        var httpClients = requestJson.createClient(basesMLabURL);
                        winston.info("HTTP client created succesfully");

                        httpClients.get("credential?" + mLabAPIKey,
                              function(err, resMLab, body) {
                                if (err) {
                                  winston.error("Error creating user");
                                  var response = { "msg" : "Error creating user" };
                                  res.status(500);
                                  res.send(response);
                                } else {
                                  var maxCredentialId = 0;
                                  for (const credential of body) {
                                    if (parseInt(credential.credentialid) > maxCredentialId) {
                                      maxCredentialId = parseInt(credential.credentialid);
                                    }
                                  }

                                  var newCredential = {
                                    "credentialid"  : parseInt(maxCredentialId + 1),
                                    "userid"        : parseInt(maxUserId + 1),
                                    "password"      : authenticationController.hash(req.body.password)
                                  }

                                  httpClients.post("credential?"  + mLabAPIKey, newCredential,
                                        function(err, resMLab, body) {
                                          if (err) {
                                            winston.error("Error creating user");
                                            var response = { "msg" : "Error creating user" };
                                            res.status(500);
                                          } else {
                                            winston.info("Credential succesfully created");
                                            var response = { "msg" : "User succesfully created",
                                                             "userid" : maxUserId + 1 };
                                          }
                                          res.send(response);
                                        })
                                }
                              })
                      }
                    })
                }
          }
        })
}


// POST Actualizacion de usuario por userid
function updateUserV1(req, res) {
  winston.info("POST /blockbank/v1/user/:userid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.userid;
  var query = 'q={"userid":' + id + '}';
  httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting user");
            var response = { "msg" : "Error getting user" };
            res.status(500);
            res.send(response);
          } else {
            if (body.length == 0) {
              winston.warn("User not found");
              var response = { "msg" : "User not found" };
              res.status(404);
              res.send(response);
            } else {
              query = 'q={"userid" :' + body[0].userid + '}';

              var putBody = {
                  	"$set" : {
                  		  "name"       : req.body.name,
                      	"surname"    : req.body.surname,
                      	"address"    : req.body.address,
                      	"cellphone"  : req.body.cellphone,
                      	"email"      : req.body.email,
                      	"timezone"   : req.body.timezone,
                      	"languaje"   : req.body.languaje,
                        "connected"  : req.body.connected,
                        "active"     : req.body.active
                  	}
              }

              httpClient.put("user?" + query + "&" + mLabAPIKey, putBody,
                    function(errPUT, resMLabPUT, bodyPUT) {
                      if (err) {
                        winston.error("Error updating user");
                        var response = { "msg" : "Error updating user" };
                        res.status(500);
                        res.send(response);
                      } else {
                        winston.info("User succesfully updated");

                        var httpClients = requestJson.createClient(basesMLabURL);
                        winston.info("HTTP client created succesfully");

                        query = 'q={"userid" :' + id + '}';
                        var putCredential = {"$set" : { "password" : authenticationController.hash(req.body.password) }};
                        httpClients.put("credential?" + query + "&" + mLabAPIKey, putCredential,
                              function(err, resMLab, body) {
                                if (err) {
                                  winston.error("Error updating user");
                                  var response = { "msg" : "Error updating user" };
                                  res.status(500);
                                } else {
                                  winston.info("Credential succesfully updated");
                                  var response = { "msg" : "User succesfully updated" };
                                }
                                res.send(response);
                        })
                      }
                })
            }
          }
        })
}


// DELETE Borrado (baja logica) de usuario por userid
function deleteUserByIdV1(req, res) {
  winston.info("DELETE /blockbank/v1/user/:userid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.userid;
  var query = 'q={"userid":' + id + '}';
  httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting user");
            var response = { "msg" : "Error getting user" };
            res.status(500);
            res.send(response);
          } else if (body.length == 0) {
            winston.error("User does not exists");
            var response = { "msg" : "User does not exists" };
            res.status(500);
            res.send(response);
          } else {
            query = 'q={"userid" :' + body[0].userid + '}';
            var putBody = {"$set" : {"active" : false}};
            httpClient.put("user?" + query + "&" + mLabAPIKey, putBody,
                  function(errPUT, resMLabPUT, bodyPUT) {
                    winston.info("User succesfully deleted");
                    var response = { "msg" : "User succesfully deleted" };
                    res.send(response);
                  }
            )
          }
        }
    )
}



// Exports de endpoints
module.exports.getUserListV1 = getUserListV1;
module.exports.getUserByIdV1 = getUserByIdV1;
module.exports.createUserV1 = createUserV1;
module.exports.updateUserV1 = updateUserV1;
module.exports.deleteUserByIdV1 = deleteUserByIdV1;
