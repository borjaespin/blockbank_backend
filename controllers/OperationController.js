// Definición de dependencias
const nconf = require('nconf');
const requestJson = require('request-json');
const moment = require('moment');
const winston = require('./../config/winston');

// Definición de controladores
const cryptocurrencyController = require('./CryptocurrencyController.js');
const accountController = require('./AccountController.js');


// Configuración conexión mLab
nconf.file({ file: './config/bbconfig.json' });
const baseMLabURL = nconf.get('mLab:baseMLabURL');
const mLabAPIKey = nconf.get('mLab:mLabAPIKey');



// GET Listado de operaciones existentes
function getOperationListV1(req, res) {
  winston.info("GET /blockbank/v1/operation");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  httpClient.get("operation?" + mLabAPIKey,
        function(err, resMLab, body) {
          var response = !err ? body : { "msg" : "Error getting operations" };
          res.send(response);
        }
      )
}


// GET Listado de una operación por operationid
function getOperationByIdV1(req, res) {
  winston.info("GET /blockbank/v1/operation/:operationid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.operationid;
  var query = 'q={"operationid":' + id + '}';
  httpClient.get("operation?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting operation");
            var response = { "msg" : "Error getting operation" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body[0];
            } else {
              winston.warn("Operation not found");
              var response = { "msg" : "Operation not found" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}


// GET Listado de operaciones por userid
function getOperationsByUserV1(req, res) {
  winston.info("GET /blockbank/v1/operationsuser/:userid");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var id = req.params.userid;
  var query = 'q={"userid":' + id + '}';
  httpClient.get("operation?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting operations");
            var response = { "msg" : "Error getting operations" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body;
            } else {
              winston.warn("User does not have any operations");
              var response = { "msg" : "User does not have any operations" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}


// GET Listado de operaciones por cryptocurrency
function getOperationsByCryptocurrencyV1(req, res) {
  winston.info("GET /blockbank/v1/operationscryptocurrency/:cryptocurrency");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var ccparam = req.params.cryptocurrency;
  if (!cryptocurrencyController.isNumericParam(req.params.cryptocurrency))
  {
    ccparam = cryptocurrencyController.convertCryptocurrencyToId(ccparam);
  }

  var query = 'q={"cryptocurrencyid":' + ccparam + '}';
  httpClient.get("operation?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting operations");
            var response = { "msg" : "Error getting operations" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body;
            } else {
              winston.warn("User does not have any operations");
              var response = { "msg" : "User does not have any operations" };
              res.status(404);
            }
          }
          res.send(response);
        }
      )
}


// GET Listado de operaciones por userid y cryptocurrency
function getOperationsByUserCryptocurrencyV1(req, res) {
  winston.info("GET /blockbank/v1/operationsusercryptocurrency/:userid/:cryptocurrency");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var ccparam = req.params.cryptocurrency;
  if (!cryptocurrencyController.isNumericParam(req.params.cryptocurrency))
  {
    ccparam = cryptocurrencyController.convertCryptocurrencyToId(ccparam);
  }

  var uid = req.params.userid;
  var query = 'q={"userid":' + uid + ',"cryptocurrencyid":' + ccparam + '}';
  httpClient.get("operation?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting operations");
            var response = { "msg" : "Error getting operations" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body;
            } else {
              winston.warn("User does not have that cryptocurrency");
              var response = { "msg" : "User does not have that cryptocurrency" };
              res.status(404);
            }
          }
          res.send(response);
        }
    )
}


// GET Listado de operaciones por userid y type
function getOperationsByUserTypeV1(req, res) {
  winston.info("GET /blockbank/v1/operationsusertype/:userid/:type");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var uid = req.params.userid;
  var type = req.params.type;
  var query = 'q={"userid":' + uid + ',"type":"' + type + '"}';
  httpClient.get("operation?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting operations");
            var response = { "msg" : "Error getting operations" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body;
            } else {
              winston.warn("User does not have that kind of operation");
              var response = { "msg" : "User does not have that kind of operation" };
              res.status(404);
            }
          }
          res.send(response);
        }
    )
}


// GET Listado de operaciones por userid, cryptocurrency y type
function getOperationsByUserCryptocurrencyTypeV1(req, res) {
  winston.info("GET /blockbank/v1/operation/:userid/:cryptocurrency/:type");

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  var ccparam = req.params.cryptocurrency;
  if (!cryptocurrencyController.isNumericParam(req.params.cryptocurrency))
  {
    ccparam = cryptocurrencyController.convertCryptocurrencyToId(ccparam);
  }

  var uid = req.params.userid;
  var type = req.params.type;
  var query = 'q={"userid":' + uid +
                  ',"cryptocurrencyid":' + ccparam +
                  ',"type":"' + type + '"}';
  httpClient.get("operation?" + query + "&" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting operations");
            var response = { "msg" : "Error getting operations" };
            res.status(500);
          } else {
            if (body.length > 0) {
              var response = body;
            } else {
              winston.warn("User does not have that kind of operation");
              var response = { "msg" : "User does not have that kind of operation" };
              res.status(404);
            }
          }
          res.send(response);
        }
    )
}


// POST Alta de nueva operacion de usuario
function createOperationUserV1(req, res) {
  winston.info("POST /blockbank/v1/operation/:userid");
  winston.info(req.headers);

  var httpClient = requestJson.createClient(baseMLabURL);
  winston.info("HTTP client created succesfully");

  httpClient.get("operation?" + mLabAPIKey,
        function(err, resMLab, body) {
          if (err) {
            winston.error("Error getting operation");
            var response = { "msg" : "Error creating operation" };
            res.status(500);
          } else {
            var maxOperationtId = 0;
            for (const operation of body) {
              if (parseInt(operation.operationid) > maxOperationtId) {
                maxOperationtId = parseInt(operation.operationid);
              }
            }

            var newOperation = {
              "operationid"       : parseInt(maxOperationtId + 1),
              "userid"            : parseInt(req.params.userid),
              "cryptocurrencyid"  : req.body.cryptocurrencyid,
              "type"              : req.body.type,
              "amount"            : req.body.amount,
              "timestamp"         : moment(Date.now()).format("YYYY-MM-DD HH:mm:ss")
            };

            httpClient.post("operation?"  + mLabAPIKey, newOperation,
                  function(err, resMLab, body) {
                    if (err) {
                      winston.error("Error creating operation");
                      var response = { "msg" : "Error creating operation" };
                      res.status(500);
                    } else {
                      winston.info("Operation succesfully created");
                      var response = { "msg" : "Operation succesfully created" };
                    }
                    res.send(response);
                  }
            )
          }
        }
      )
}


// Exports de endpoints
module.exports.getOperationListV1 = getOperationListV1;
module.exports.getOperationByIdV1 = getOperationByIdV1;
module.exports.getOperationsByUserV1 = getOperationsByUserV1;
module.exports.getOperationsByCryptocurrencyV1 = getOperationsByCryptocurrencyV1;
module.exports.getOperationsByUserCryptocurrencyV1 = getOperationsByUserCryptocurrencyV1;
module.exports.getOperationsByUserTypeV1 = getOperationsByUserTypeV1;
module.exports.getOperationsByUserCryptocurrencyTypeV1 = getOperationsByUserCryptocurrencyTypeV1;
module.exports.createOperationUserV1 = createOperationUserV1;
