// Definición de dependencias
const mocha = require ('mocha');
const chai = require ('chai');
const chaihttp = require ('chai-http');
const nconf = require('nconf');
const winston = require('./../config/winston');


chai.use(chaihttp);

var should = chai.should();
var server = require('../server');

nconf.file({ file: './config/bbconfig.json' });
var port = nconf.get('http:port');



// Comprobando acceso a Internet
describe('--> INTERNET UP & RUNNING',
	function() {
		// Testing connectivity to Google
		it('Testing Google', function(done) {
			chai.request('https://www.google.com')
			.get('/')
			.end(
				function(err, res) {
					winston.debug("Google Request has finished");
					res.should.have.status(200);
					done();
				}
			)
		}),
    // Testing connectivity to Duck Duck Go
    it('Testing Duck Duck Go', function(done) {
			chai.request('http://www.duckduckgo.com')
			.get('/')
			.end(
				function(err, res) {
					winston.debug("Duck Duck Go Request has finished");
					res.should.have.status(200);
					done();
				}
			)
		})
	}
)


// Comprobando APIs remotas
describe('--> REMOTE APIS UP & RUNNING',
	function() {
		// Testing connectivity to mLab
		it('Testing mLab', function(done) {
			chai.request('https://mlab.com/')
			.get('/')
			.end(
				function(err, res) {
					winston.debug("mLab Request has finished");
					res.should.have.status(200);
					done();
				}
			)
		}),
		// Testing connectivity to mLab API & Collections
    it('Testing mLab API & Collections', function(done) {
      chai.request('https://api.mlab.com/api/1')
      .get('/databases?apiKey=deyXSJ7wCGU_Z631Fw4-w1zn5p82tkNu')
      .end(
        function(err, res) {
          winston.debug("mLab API Collections Request has finished");
          res.should.have.status(200);
          done();
        }
      )
    })
	}
)


// Comprobando APIs localhost de BlockBank
describe('--> LOCAL BLOCKBANK UP & RUNNING',
	function() {
		// Testing connectivity to BlockBank
		it('Testing BlockBank Users', function(done) {
			chai.request('localhost:'+ port +'/blockbank/v1')
			.get('/user')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (user of res.body) {
						user.should.have.property('userid');
						user.should.have.property('name');
						user.should.have.property('surname');
						user.should.have.property('address');
						user.should.have.property('cellphone');
						user.should.have.property('email');
						user.should.have.property('timezone');
						user.should.have.property('languaje');
						user.should.have.property('connected');
						user.should.have.property('active');
					}
					done();
				}
			)
		}),
		it('Testing BlockBank Credentials', function(done) {
			chai.request('localhost:'+ port +'/blockbanks/v1')
			.get('/credential')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (account of res.body) {
						account.should.have.property('credentialid');
						account.should.have.property('userid');
						account.should.have.property('password');
					}
					done();
				}
			)
		}),
		it('Testing BlockBank Accounts', function(done) {
			chai.request('localhost:'+ port +'/blockbank/v1')
			.get('/account')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (account of res.body) {
						account.should.have.property('accountid');
						account.should.have.property('userid');
						account.should.have.property('cryptocurrencyid');
						account.should.have.property('address');
						account.should.have.property('balance');
						account.should.have.property('active');
					}
					done();
				}
			)
		}),
		it('Testing BlockBank Cryptocurrencies', function(done) {
			chai.request('localhost:'+ port +'/blockbank/v1')
			.get('/cryptocurrency')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (cryptocurrency of res.body) {
						cryptocurrency.should.have.property('cryptocurrencyid');
						cryptocurrency.should.have.property('name');
						cryptocurrency.should.have.property('shortname');
						cryptocurrency.should.have.property('eurvalue');
					}
					done();
				}
			)
		}),
		it('Testing BlockBank Operations', function(done) {
			chai.request('localhost:'+ port +'/blockbank/v1')
			.get('/operation')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (operation of res.body) {
						operation.should.have.property('operationid');
						operation.should.have.property('userid');
						operation.should.have.property('cryptocurrencyid');
						operation.should.have.property('type');
						operation.should.have.property('amount');
						operation.should.have.property('timestamp');
					}
					done();
				}
			)
		})
	}
)


// Comprobando APIs Openshift de BlockBank
describe('--> OPENSHIFT BLOCKBANK UP & RUNNING',
	function() {
		// Testing connectivity to BlockBank
		it('Testing BlockBank Users', function(done) {
			chai.request('http://blockbank-blockbank.a3c1.starter-us-west-1.openshiftapps.com/blockbank/v1')
			.get('/user')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (user of res.body) {
						user.should.have.property('userid');
						user.should.have.property('name');
						user.should.have.property('surname');
						user.should.have.property('address');
						user.should.have.property('cellphone');
						user.should.have.property('email');
						user.should.have.property('timezone');
						user.should.have.property('languaje');
						user.should.have.property('connected');
						user.should.have.property('active');
					}
					done();
				}
			)
		}),
		it('Testing BlockBank Credentials', function(done) {
      chai.request('http://blockbank-blockbank.a3c1.starter-us-west-1.openshiftapps.com/blockbanks/v1')
			.get('/credential')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (account of res.body) {
						account.should.have.property('credentialid');
						account.should.have.property('userid');
						account.should.have.property('password');
					}
					done();
				}
			)
		}),
		it('Testing BlockBank Accounts', function(done) {
      chai.request('http://blockbank-blockbank.a3c1.starter-us-west-1.openshiftapps.com/blockbank/v1')
			.get('/account')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (account of res.body) {
						account.should.have.property('accountid');
						account.should.have.property('userid');
						account.should.have.property('cryptocurrencyid');
						account.should.have.property('address');
						account.should.have.property('balance');
						account.should.have.property('active');
					}
					done();
				}
			)
		}),
		it('Testing BlockBank Cryptocurrencies', function(done) {
			chai.request('http://blockbank-blockbank.a3c1.starter-us-west-1.openshiftapps.com/blockbank/v1')
			.get('/cryptocurrency')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (cryptocurrency of res.body) {
						cryptocurrency.should.have.property('cryptocurrencyid');
						cryptocurrency.should.have.property('name');
						cryptocurrency.should.have.property('shortname');
						cryptocurrency.should.have.property('eurvalue');
					}
					done();
				}
			)
		}),
		it('Testing BlockBank Operations', function(done) {
			chai.request('http://blockbank-blockbank.a3c1.starter-us-west-1.openshiftapps.com/blockbank/v1')
			.get('/operation')
			.end(
				function(err, res) {
					winston.debug("Blockbank Request has finished");
					res.should.have.status(200);
					res.body.length.should.be.gt(0);
					res.body.should.be.a("array");
					for (operation of res.body) {
						operation.should.have.property('operationid');
						operation.should.have.property('userid');
						operation.should.have.property('cryptocurrencyid');
						operation.should.have.property('type');
						operation.should.have.property('amount');
						operation.should.have.property('timestamp');
					}
					done();
				}
			)
		})
	}
)
