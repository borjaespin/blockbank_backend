// Definición de dependencias
const express = require('express');
const bodyParser = require('body-parser');
const nconf = require('nconf');
const winston = require('./config/winston');

// Definición de controladores
const sessionController = require('./controllers/SessionController');
const userController = require('./controllers/UserController');
const accountController = require('./controllers/AccountController');
const cryptocurrencyController = require('./controllers/CryptocurrencyController');
const operationController = require('./controllers/OperationController');
const authenticationController = require('./controllers/AuthenticationController');


// Configuración de la aplicación
nconf.file({ file: './config/bbconfig.json' });
nconf.defaults({ 'http': {  'port': 3000  } });
const port = process.env.PORT || nconf.get('http:port');


// Definición y arranque
const app = express();
app.use(bodyParser.json());
app.listen(port);
winston.info("API listening at port " + port);


//Tablas de enrutamiento de sesión
app.post('/blockbank/v1/login', sessionController.loginV1);
app.post('/blockbank/v1/logout', sessionController.logoutV1);
// Tablas de enrutamiento de usuario
app.get('/blockbank/v1/user', userController.getUserListV1);
app.get('/blockbank/v1/user/:userid', userController.getUserByIdV1);
app.post('/blockbank/v1/user', userController.createUserV1);
app.post('/blockbank/v1/user/:userid', userController.updateUserV1);
app.delete('/blockbank/v1/user/:userid', userController.deleteUserByIdV1);
// Tablas de enrutamiento de credenciales
app.get('/blockbanks/v1/credential', authenticationController.getCredentialsV1);
app.get('/blockbanks/v1/credential/:userid', authenticationController.getCredentialByUserIdV1);
// Tablas de enrutamiento de cuenta
app.get('/blockbank/v1/account', accountController.getAccountListV1);
app.get('/blockbank/v1/account/:accountid', accountController.getAccountByIdV1);
app.get('/blockbank/v1/accountsuser/:userid', accountController.getAccountsByUserV1);
app.get('/blockbank/v1/accountusercryptocurrency/:userid/:cryptocurrency', accountController.getAccountByUserCryptocurrencyV1);
app.post('/blockbank/v1/accountusercryptocurrency/:userid/:cryptocurrency', accountController.createAccountByUserCryptocurrencyV1);
app.post('/blockbank/v1/account/:accountid', accountController.updateAccountV1);
app.post('/blockbank/v1/accountusercryptocurrency/:userid/:cryptocurrency/:balance', accountController.updateAccountByUserCryptocurrencyV1);
app.delete('/blockbank/v1/account/:accountid', accountController.deleteAccountByIdV1);
app.delete('/blockbank/v1/accountsuser/:userid', accountController.deleteAccountsByUserV1);
// Tablas de enrutamiento de cryptomoneda
app.get('/blockbank/v1/cryptocurrency', cryptocurrencyController.getCryptocurrencyListV1);
app.get('/blockbank/v1/cryptocurrency/:cryptocurrency', cryptocurrencyController.getCryptocurrencyV1);
app.post('/blockbank/v1/cryptocurrency/:cryptocurrency', cryptocurrencyController.updateCryptocurrencyV1);
//Tablas de enrutamiento de operación
app.get('/blockbank/v1/operation', operationController.getOperationListV1);
app.get('/blockbank/v1/operation/:operationid', operationController.getOperationByIdV1);
app.get('/blockbank/v1/operationsuser/:userid', operationController.getOperationsByUserV1);
app.get('/blockbank/v1/operationscryptocurrency/:cryptocurrency', operationController.getOperationsByCryptocurrencyV1);
app.get('/blockbank/v1/operationsusercryptocurrency/:userid/:cryptocurrency', operationController.getOperationsByUserCryptocurrencyV1);
app.get('/blockbank/v1/operationsusertype/:userid/:type', operationController.getOperationsByUserTypeV1);
app.get('/blockbank/v1/operation/:userid/:cryptocurrency/:type', operationController.getOperationsByUserCryptocurrencyTypeV1);
app.post('/blockbank/v1/operation/:userid', operationController.createOperationUserV1);
